Le qualificateur de type **volatile** en C
==================================

/* TODO :
 * Insérer le code assembleur de GCC
 * Détailler le code de manière plus importante
 * Insérer la partie sur les recommandations du Kernel
 * 
 */

Le qualificateur de type **volatile** présent en C s'applique à une variable ou à un pointeur. Celui-ci stipule que la valeur de la variable (ou du pointeur) peut changer sans que le compilateur ne le sache pour autant.

Selon le standard :
>An object that has volatile-qualified type may be modified in ways unknown to the implementation or have other unknown side effects. Therefore any expression referring to such an object shall be evaluated strictly according to the rules of the abstract machine, as described in 5.1.2.3. Furthermore, at every sequence point the value last stored in the object shall agree with that prescribed by the abstract machine, except as modified by the unknown factors mentioned previously. What constitutes an access to an object that has volatile-qualified type is implementation-defined.

De manière plus claire : un objet ayant le qualificateur de type **volatile** peut être modifiée sans que l'implémentation (ici, le compilateur) ne le sache, ou même avoir certains effets de bord. Ainsi, le compilateur ne doit pas tenter d'optimiser ce type de variables. Malgré cela, le qualificateur de type **volatile** possède un comportement définit par l'implémentation, donc par le compilateur.

##### _Pourquoi utiliser **volatile** ?_

Il y a plusieurs raisons pouvant amener à l'utilisation de ce mot-clé, tout d'abord l'accès à des périphériques mappées en mémoire, mais également l'accès concurrent, entre deux threads différents par exemple. 

L'optimisation est donc désactivée lors de la gestion de variables **volatile**, mais, concrètement, qu'est-ce que ça change ? Prenons un exemple :

~~~c
int main(void)
{
    int foo = 3;
    
    while (foo != 0)
        ;

    return 0;
}
~~~

Ici, nous déclarons un entier `foo`, puis, tant que cet entier est différent de 0, nous bouclons.

_Mais **foo** ne changera jamais, non ?_

C'est pas vrai. Mais ce n'est pas vrai pour autant. Dans le cas présent, il est vrai que `foo` ne changera pas, mais on peut suppose que `foo` est une ressource partagée entre deux threads différents, auquel cas le second threads peut tout à faire modifer `foo` au cours de son exécution.

_D'accord. **foo** va changer. Et alors ?_

En l'état, le code ne pose pas vraiment de problème. Mais comme indiqué plus haut, **volatile** agit sur les optimisations du compilateur. Dans ce cas, voyons le code assembleur généré par GCC :

~~~asm

~~~

Ici, on remarque effectivement le problème. En effet, la valeur présente dans `foo` n'est pas analysée à chaque début de boucle. La finalité est effectivement la même qui si la valeur était analysée, or, si cette valeur change, le prgramme de le saura pas et il sera impossible de sortir de la boucle infinie.

Ainsi, si maintenant nous déclarons `foo` en tant que **volatile** :

~~~c
int main(void)
{
    volatile int foo = 3;
    
    while (foo != 0)
        ;

    return 0;
}
~~~

Ainsi, de cette manière, le contenu de la variable `foo` est déclaré comme **volatile**, le compilateur ne l'optimisera pas

~~~asm
~~~

On voit bien que, désormais, la valeur de `foo` est analysée à chaque début de boucle. Si cette valeur est changée pour 0, la boucle s'arrêtera !

##### _Qu'en penses les développeurs Kernel ?_

A vrai dire, il n'aiment pas vraiment. Ils signalent que, en général, **volatile** est utilisé comme signifiant que la variable pouvait être changée en dehors de son thread d'éxécution, l'utilisant ainsi pour déclarer des structures partagées, comme si les données étaient atomiques. Sachant que la protection des données contre l'accès concurrent est très différent du fait que le compilateur n'optimise pas la variable (ce qui est fournit par **volatile**).

De plus, il se trouve de des mécanisme de protection d'accès concurrent sont déjà disponibles dans le kernel 
tels que [les mutex](https://fr.wikipedia.org/wiki/Exclusion_mutuelle), [les spinlocks](https://fr.wikipedia.org/wiki/Spinlock) ou [les barrières mémoire](https://en.wikipedia.org/wiki/Memory_barrier). Ces mécanisme se chargent eux même de manipuler les optimisations du compilateurs afin de pouvoir fonctionner de manière optimale. Ainsi, l'utilisation de **volatile** dans ce contexte est superflu.

Si jamais l'utilisation de **volatile** est malgré tout faite dans les condition présentées plus haut, lors d'un accès concurrent, dans ce cas, ce mot clé empêcherait l'optimisation des variables par le compilateur à l'intérieur du spinlock ou mutex. Cela serait inutile, étant donné que l'accès atomique est déjà garantit.

Donc concrètement, l'utilisation de **volatile** se fait de la même manière que les `typedef`, à savoir lorsque toutes les conditions sont réunies, et lorsqu'il n'y a pas d'autre alternatives.

Et pour citer Linux Torvalds :

>If the memory barriers aren't sufficient, the volatiles are useless. If the memory barriers _are_ sufficient, the volatiles are useless.
>
>See? They're useless.

##### _Pourquoi j'en parle ?_

L'idée de faire ce post me vient d'un petit problème que j'ai eu en écrivant mon kernel. En effet, j'avais besoin d'écrire des informations à l'écran lors des premières phases de boot du kernel. Pour ce faire, il existe un moyen assez simple en assembleur. En effet, il se trouve que la mémoire vidéo est mappée à l'adresse 0xB8000 (mémoire vidéo, mappée, ça commence à percuter ? ;)).

Cette mémoire vidéo permet l'affichage de caractères sur une console de 25 lignes par 80 colonnes. Chaque caractère est représenté par 2 octets, le premier pour le caractère en lui même, le second pour ses attributs, formatés de cette manière :

~~~
|  0  |  1  |  2  |  3  |  4  |  5  |  6  |  7  |
|-----|-----|-----|-----|-----|-----|-----|-----|
| cli |  backgr. color  | int |   font color    |
~~~

Avec `cli` pour clignotement, `int` pour sur-intensité, et pour les couleurs :
>`000` noir  
>`001` bleu  
>`010` vert  
>`011` cyan  
>`100` rouge  
>`101` magenta  
>`110` jaune  
>`111` blanc  

Ainsi, si nous faisons :

~~~asm
mov 0x8A000, 0x41 ; 'A'
mov 0x8A001, 0x81 ; Soit 10000010b
~~~

Nous obtenons alors, en haut à gauche de notre écran, un manifique 'A' vert sur fond noir clignotant ! C'est laid, mais ça marche ;)

En assembleur c'est sympa, ça marche, mais c'est assez fastidieux. Et puis, si nous voulons écrire une phrase entière, cela risque d'être très long ! Il me faut donc un appel système, `print()`, qui permettera d'afficher une chaîne de caractères entière ! 

~~~c
char sX = 0;

void print(char *s)
{
    while (*s != 0)
        putcar(*s++);
}

void putcar(unsigned char c)
{
    unsigned char *video = (unsigned char *)(0xB8000);
    video = (unsigned char *)(video + 2 * sX);
    
    *video = c;
    *(video + 1) = 0x82; /* Parce qu'on aime le vert sur fond noir */
    sX++;
}
~~~

Voilà, naïvement ce que nous pouvons faire. Ici, pour simplifier les choses, nous nous contenterons d'écrire sur la première ligne, les caractères les uns à la suite des autres. Nous déclarons alors un pointeur vers le début de la mémoire vidéo mappée, à ce pointeur nous ajoutons alors le nombre de caractères écrits (pour écrire à la suite) multipié par 2, car il ne faut pas oublier que chaque caractère est représenté par 2 octets ! Ensuite, à l'adresse obtenue nous copions le caractère, puis ajoutons les paramtères d'affichage de ce caractères à l'adresse suivante ! Enfin, nous pouvons incrémenter le compteur de caractères :) 

Voilà ce que moi-même j'avais écrit lors de l'implémentation de cette fonction. Le problème présent est que cela ne fonctionne pas ! En effet, de par les optimisations effectuée par GCC sur mes variables, il se trouve que l'adresse présente dans `video` n'est pas l'adresse à laquelle j'essaye d'accèder :/ Mais malgré tout, nous savons maintenant corriger le problème, grâce à **volatile** ! 

~~~c
char sX = 0;

void print(char *s)
{
    while (*s != 0)
        putcar(*s++);
}

void putcar(unsigned char c)
{
    volatile unsigned char *video = (volatile unsigned char *)(0xB8000);
    video = (volatile unsigned char *)(video + 2 * sX);
    
    *video = c;
    *(video + 1) = 0x82; /* Parce qu'on aime le vert sur fond noir */
    sX++;
}
~~~ 

Ici, le problème n'est plus, tout fonctionne ! En effet, `video` est maintenant déclarée comme étant **volatile**, nous pouvons alors la modifier aisément. L'accès à la mémoire mappée fonctionne et, de fait, l'affichage également !

kernel

----

Bibliographie:  
["volatile (computer programming)" - Wikipédia](https://en.wikipedia.org/wiki/Volatile_%28computer_programming%29#cite_note-1)  
["Writing to video memory (0xB8000) & volatile pointer" - StackOverflow](http://stackoverflow.com/questions/32221707/writing-to-video-memory-0xb8000-volatile-pointer?noredirect=1#comment52330856_32221707)  
["Why is volatile needed in C" - StackOverflow](http://stackoverflow.com/questions/246127/why-is-volatile-needed-in-c)  
["How to Use C's volatile Keyword" - BarrGroup](http://www.barrgroup.com/Embedded-Systems/How-To/C-Volatile-Keyword)  
["Const and volatile" - gbdirect](http://publications.gbdirect.co.uk/c_book/chapter8/const_and_volatile.html)  
["Why the "volatile" type class should not be used" - kernel.org](https://www.kernel.org/doc/Documentation/volatile-considered-harmful.txt)  
["[RFC/PATCH] doc: volatile considered evil" - lwn.net](https://lwn.net/Articles/233482/)  